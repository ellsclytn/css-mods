const fs = require("fs");
const postcss = require("postcss");
const scss = require("postcss-scss");

const rhythmGenerator = definition => {
  const value = parseInt(definition.replace("px", ""), 10);

  if (value < -8 || value > 8) {
    return Math.floor(value / 8);
  } else {
    return Math.round(value / 4) * 0.5;
  }
};

const spacingSystem = postcss.plugin("postcss-backwards", opts => {
  return function(css, result) {
    css.walkDecls(declaration => {
      if (
        declaration.prop.includes("border") ||
        declaration.prop.includes("font-size")
      ) {
        return;
      }
      const pixelDefinitions = declaration.value.match(/(\d*\.?\d*)px/g);

      pixelDefinitions.forEach(definition => {
        declaration.value = declaration.value.replace(
          definition,
          `rhythm(${rhythmGenerator(definition)})`
        );
      });
    });
  };
});

fs.readFile("thing.scss", (err, css) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }

  postcss([spacingSystem])
    .process(css, { syntax: scss })
    .then(result => {
      console.log(result.css);
    });
});
